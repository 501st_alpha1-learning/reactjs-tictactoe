import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

/**
 * Render a square with the proper value.
 *
 * @param {Object}   props
 * @param {function} props.onClick  Action to run on click.
 * @param {string}   props.value    String to display in square
 *                                  (usually 'X' or 'O').
 * @param {boolean}  props.win      If true, add class 'win' to square.  Used
 *                                  for decorating squares that were part of the
 *                                  win.
 *
 * @return React component for the Square.
 */
function Square(props) {
  let className = "square";
  if (props.win) {
    className += " win";
  }

  return (
    <button className={className} onClick={props.onClick}>
      {props.value}
    </button>
  );
}

/**
 * Build a Tic-Tac-Toe board of 3x3 squares.
 */
class Board extends React.Component {
  /**
   * Render the given square.
   *
   * State pulled from `this.props` and passed to child object.
   *
   * @param {number} i  The square to render, where `0` is upper left, `1` is
   *                    upper middle, and so on.
   *
   * @return React component for the Square.
   */
  renderSquare(i) {
    let win = false;

    // If this square is part of winning set, pass that info to sub component.
    if (this.props.winningSquares) {
      for (let j = 0; j < this.props.winningSquares.length; j++) {
        if (this.props.winningSquares[j] === i) {
          win = true;
          break;
        }
      }
    }

    return (
      <Square
        value={this.props.squares[i]}
        win={win}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  /**
   * Render the entire 3x3 board.
   *
   * @return React component for the Board.
   */
  render() {
    let rows = [];
    for (let i = 0; i < 3; i++) {
      let cols = [];
      for (let j = 0; j < 3; j++) {
        let index = (i * 3) + j;
        cols.push(
          <span key={j}>
            {this.renderSquare(index)}
          </span>
        );
      }
      rows.push(
        <div key={i} className="board-row">
          {cols}
        </div>
      );
    }

    return (
      <div>
        {rows}
      </div>
    );
  }
}

/**
 * Get row and column for given move.
 *
 * @param {Array}  history  The current history of moves.
 * @param {number} move     The number of the current move, to be compared with
 *                          previous move.
 *
 * @return {Array}  Row of move in first position, column in second position.
 */
function getMovePosition(history, move) {
  if (move < 1)
    return [null, null];

  const last = history[move - 1].squares;
  const current = history[move].squares;

  let row, col;
  for (let i = 0; i < 9; i++) {
    if (current[i] !== last[i]) {
      row = Math.floor(i / 3);
      col = i % 3;
      break;
    }
  }

  return [row, col];
}

/**
 * Build and manage the Tic-Tac-Toe game.
 */
class Game extends React.Component {
  /**
   * Set default state for game.
   */
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true,
      orderDescending: false,
    };
  }

  /**
   * Handle a click on the given square.
   *
   * @param {number} i  The square that was clicked.
   */
  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();

    // If game over or square already filled, do nothing.
    if (squares[i] || calculateWinner(squares)[0]) {
      return;
    }

    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  /**
   * Jump to the given step in the history.
   *
   * @param {number} step  The step to jump to.
   */
  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    });
  }

  /**
   * Handle a click on the sort button: invert history sort.
   */
  handleSortClick() {
    this.setState({
      orderDescending: !this.state.orderDescending,
    });
  }

  /**
   * Handle a click on the reset button: reset game state.
   */
  handleResetClick() {
    this.setState({
      history: [{
        squares: Array(9).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true,
    });
  }

  /**
   * Render the game board and other info.
   *
   * @return React component for the game.
   */
  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const [winner, winningSquares] = calculateWinner(current.squares);

    const moves = history.map((step, index) => {
      let move = index;
      if (this.state.orderDescending) {
        move = history.length - move - 1;
      }
      const [row, col] = getMovePosition(history, move);
      let desc = move
               ? 'Go to move #' + move + ' (' + row + ',' + col + ')'
               : 'Go to game start';

      if (move === this.state.stepNumber) {
        desc = (
          <strong>{desc}</strong>
        );
      }

      return (
        <li key={index}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    if (winner) {
      status = 'Winner: ' + winner;
    }
    else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }

    const sort = this.state.orderDescending ? 'Descending' : 'Ascending';

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            winningSquares={winningSquares}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <div>
            <button onClick={() => this.handleResetClick()}>New Game</button>
          </div>
          <div>
            <button onClick={() => this.handleSortClick()}>
              Toggle Move Sort: {sort}</button>
          </div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

/**
 * Calculate if there is a winner based on given squares.
 *
 * @param {Array} squares  The array of squares to check.
 *
 * @return {Array}  First element: the winner if one exists, or `null`
 *                  otherwise.  Second element: array of all winning squares, or
 *                  `null` otherwise.
 */
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  let winner = null;
  let winSquares = [];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      winner = squares[a];
      winSquares = winSquares.concat(lines[i]);
    }
  }

  if (winner) {
    return [winner, winSquares];
  }

  return [null, null];
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
